<center>

## <u>Germinal</u>

#### A gambas terminal application
'Written be Bruce Steers'
</center>

#### Main Features..

* Bookmark folders and use the bookmarks to either cd the dir or input the text
* Customize look, Color, transparency, font
* Navigate the cursor using arrow keys while holding Ctrl (left and right move to next space, up and down move line) [https://youtu.be/QBhxohum3AQ]
* Show bash history in a searchable list.
* Edit current command line in a TextEditor
* Select/Delete current command line.
* Create custom commands to manipulate the command line (or do something else)
[./help/customcommands.md](./help/customcommands.md)


