
[Back to index](./index.md)

## <u>Making custom commands...</u>

You can create commands to manipulate/edit the current command line.

Commands are made of sequences of either scripts or key commands.

Select the custom command editor from the menu to open the editor.

Press the Add button to add either a script or key sequence.

### Add a Script:
#### Add a bash or gambas script. input the script into the editor.

#### Tokens can be used in the script for the following data..\
* $(Text) = The whole command line (quoted)
* $(Selected) = Selected text (or whole command line) (quoted)
* $(SelectedOnly) = Selected text (or nothing) (quoted)
* $(Cursor) = Cursor position
* $(SelectStart) = Selected text position

#### There are 4 choices of what to do with your script output...
* Ignore ; do nothing with the command output.
* Insert at cursor position
* Overwrite selected text ; replaces selected text
* Overwrite command; replace the whole command line.

### Add key sequence...
#### choose from the following selection of key commands.\
(all commands that take a value default to 1 if no value is given)
* Delete=n ; Delete n chars to right (default is 1)
* Backspace=n ; Delete n chars to left
* Left=n, Right=n, Up=n, Down=n ; Move n spaces Left/right/up/down
* Home ; Move Home
* End ; Move End
* Copy ; Copy selected text
* Paste ; Paste clipboard
* Cursor=n ; Position cursor
* UnSelect ; Clear selection
* RestoreCursor ; Put cursor back where it was
* Positions ; Recheck $(Cursor) and $(SelectStart), They are unchanged from first check unless you use this.

### Important notes...

Scripts that either Print or echo text should not use a linefeed unless you wish for the command line to run as a linefeed will be like hitting return on your keyboard.

The Checkbox "**TrimLF**" when selected ensures that a trailing LF is removed and and other LF will be convetrted to ;

Examples
* 'Print "text";' (gambas, use a semicolon)
* 'echo -n "text"' (bash, use -n)
----
**The test terminal is a live bash terminal** so be carefull.  Ie. Be sure the command line you are testing it on is harmless and will not cause any issues if a linefeed runs the command.
----
Key sequence tokens **cannot** be used in scripts\ 
but script tokens **can** be used in key sequences.
----
To abort a script exit with an error code.

Examples..
* 'If Error Then Quit 1' (gambas)
* 'if \[ $? -ne 0 ]; then exit $?; fi' (bash)

